using UnityEngine;
using System.Collections;
using System;

public class CameraManager: MonoBehaviour
{
	// The player character to follow.
	private const string PlayerGoName = "Player";
	
	public enum CameraFormat
	{
		followTarget=0
	};
	public CameraFormat state = CameraFormat.followTarget;
	private GameObject player;
	private int xSpeed;
	private int ySpeed;
	
	// Half and quarter screen are used to measuer 50% screen points
	// And 25% points, to know when to move the camera on followtarget mode
	private Vector3 halfScreen = new Vector3 (0.5f, 0.5f, 0);
	private Vector3 quarterScreen = new Vector3 (0.25f, 0.25f, 0);
	private Vector3 halfDelta;
	private Vector3 quarterDelta;
	
	// The zeroPoint will be used to know when we got to the earliest
	// part of the map, the onePoint does the same on the other side.
	// They show the position on the Right bottom corner and left top
	// corner
	private Vector3 zeroPoint;
	private Vector3 onePoint;
	
	// The point where the camera will stop following the character 
	// (meaning the map is over)
	public int mapXLength = 1000;
	public int mapYLength = 500;
	private Vector3 destination;
	 
	void Awake ()
	{
		SetPixelResolution ();
		switch (state) {
		case CameraFormat.followTarget:
			AwakeFollowTarget ();
			break;		
		}
	}
	
	void Update ()
	{
		switch (state) {
		case CameraFormat.followTarget:
			UpdateFollowTarget ();
			break;			
		}
	}

	// Changes the screen resolution of the camera
	public void SetPixelResolution ()
	{
		int pixelSize = 1;		
		if (Screen.width >= 960 && Screen.height >= 600) {
			pixelSize = 2;	
		}
		Camera camera = gameObject.camera;

		camera.orthographic = true;

		float screenH = Screen.height;
		camera.orthographicSize = screenH / 2 / pixelSize;
	}
	
	void AwakeFollowTarget ()
	{
		// We get our target, or throw and exception if it's not set		
		GameObject playerGo = GameObject.Find (PlayerGoName);
		if (!playerGo)
			throw new Exception ("Cannot find a gameobject named " + PlayerGoName);
		player = playerGo;
		if (player)
			transform.position = player.transform.position;
	}
	
	void UpdateFollowTarget ()
	{
		if (player) {	        
			destination = Vector3.zero;
			// We want to know if our target will go farther than 50% screen and
			// less than 25%, in both up and down directions. 
			// So we'll get the delta of our target and the point of half and quarter the screen. 			
			halfDelta = player.transform.position - camera.ViewportToWorldPoint (halfScreen);						
			quarterDelta = player.transform.position - camera.ViewportToWorldPoint (quarterScreen);						


			// Basicly, if we pass the 25-50% space, the camera will update to the character new position.
			// The exception is the case we reach the borders of the map. Or if we haven't moved, 
			// then the camera wont move
			destination.x = transform.position.x;
			destination.y = transform.position.y;			
			
			if (halfDelta.x > 0)
				destination.x = transform.position.x + halfDelta.x;
			if (halfDelta.y > 0)
				destination.y = transform.position.y + halfDelta.y;	
			if (quarterDelta.x < 0)
				destination.x = transform.position.x + quarterDelta.x;
			if (quarterDelta.y < 0)
				destination.y = transform.position.y + quarterDelta.y;
			
			if (destination.y >= mapYLength || destination.y <= 0)
				destination.y = transform.position.y;
			if (destination.x >= mapXLength || destination.x <= 0)
				destination.x = transform.position.x;
			
			if (destination != Vector3.zero)
				transform.position = destination;
		}				
	}
	
}

