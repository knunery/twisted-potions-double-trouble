using UnityEngine;
using System.Collections;
using System;

public class InputManager : MonoBehaviour
{	
	private const string PlayerGoName = "Player";
	private const string LeftButtonGoName = "LeftButton";
	private const string RightButtonGoName = "RightButton";	
	private const string AButtonGoName = "AButton";
	private const string BButtonGoName = "BButton";
	
	public enum InputMode
	{
		Moving=0
	};
	public InputMode InputModeState;

	public enum Key
	{
		LeftButton=0,
		RightButton,
		AButton,
		BButton
	};
	private int buttonStatus = 0;
	private int touchStatus = 0;
	private int abTouchStatus = 0;
	private PlayerCharacter player;
	private Button leftButton;
	private Button rightButton;
	private Button aButton;
	private Button bButton;
	private RaycastHit hit;
	private Ray ray;
		
	void Awake ()
	{
		switch (InputModeState) {
		case InputMode.Moving:
			AwakeMove ();
			break;
		}
	}
	
	void Update ()
	{
		switch (InputModeState) {
		case InputMode.Moving:
			ManageMove ();
			break;
		}
	}
		
	void AwakeMove ()
	{
		GameObject playerGo = GameObject.Find (PlayerGoName);
		if (!playerGo)
			throw new Exception ("Cannot find a gameobject named " + PlayerGoName);
		player = (PlayerCharacter)playerGo.GetComponent (typeof(PlayerCharacter));
		#if UNITY_IPHONE || UNITY_ANDROID
			GameObject leftButtonGo = GameObject.Find(LeftButtonGoName);
			GameObject rightButtonGo = GameObject.Find(RightButtonGoName);		
			GameObject aButtonGo = GameObject.Find(AButtonGoName);		
			GameObject bButtonGo = GameObject.Find(BButtonGoName);		
			if (!leftButtonGo)
				throw new Exception("Cannot find a gameobject named " + LeftButtonGoName );
			if (!rightButtonGo)
				throw new Exception("Cannot find a gameobject named " + RightButtonGoName);
			if (!aButtonGo)
				throw new Exception("Cannot find a gameobject named " + AButtonGoName);
			if (!bButtonGo)
				throw new Exception("Cannot find a gameobject named " + BButtonGoName);
		
			leftButton = (Button) leftButtonGo.GetComponent(typeof(Button));
			rightButton = (Button) rightButtonGo.GetComponent(typeof(Button));		
			aButton = (Button) aButtonGo.GetComponent(typeof(Button));		
			bButton = (Button) bButtonGo.GetComponent(typeof(Button));		
		#endif
	}
		
	
	void ManageMove ()
	{
		#if UNITY_IPHONE || UNITY_ANDROID
			// We check our touch input.
			// Basically first we revert the touchstatus to 0 and then check each finger pressed 
			// and act accordingly 			
			abTouchStatus = 0;
			touchStatus = 0;
			if (Input.touchCount != 0) {
				foreach (Touch touch in Input.touches) {
					ray = Camera.main.ScreenPointToRay (touch.position);
					if (Physics.Raycast (ray, out hit)) {
						if (hit.transform.gameObject.name == LeftButtonGoName)  {
							touchStatus = -1;
						}
						else if (hit.transform.gameObject.name == RightButtonGoName) {
							touchStatus = 1;				
						}
						else if (hit.transform.gameObject.name == RightButtonGoName) {
							touchStatus = 1;				
						}
						else if (hit.transform.gameObject.name == AButtonGoName) {
							abTouchStatus = 1;				
						}
						else if (hit.transform.gameObject.name == BButtonGoName) {
							abTouchStatus = -1;				
						}					
					}
				}
			}
		#endif
		
		// Since we have all our touch input. We start getting the first button status and act accordingly 
		buttonStatus = (int)Input.GetAxis ("Horizontal");
		
		if (buttonStatus == 1 || touchStatus == 1) {
			player.PerformAction (PlayerCharacter.ActionState.WalkRight);
			#if UNITY_IPHONE || UNITY_ANDROID
				leftButton.Press(Button.ButtonStatus.Normal);
				rightButton.Press(Button.ButtonStatus.Pressed);		
			#endif
		} else if (buttonStatus == -1 || touchStatus == -1) {
			player.PerformAction (PlayerCharacter.ActionState.WalkLeft);
			#if UNITY_IPHONE || UNITY_ANDROID
				leftButton.Press(Button.ButtonStatus.Pressed);
				rightButton.Press(Button.ButtonStatus.Normal);
			#endif
		} if (buttonStatus == 0 && touchStatus == 0) {
			player.PerformAction (PlayerCharacter.ActionState.Stand);	
			#if UNITY_IPHONE || UNITY_ANDROID
				leftButton.Press(Button.ButtonStatus.Normal);
				rightButton.Press(Button.ButtonStatus.Normal);
			#endif

		}

		// To save a bit of memory we reuse the buttonstatus (unlike we do with touch input due 
		// to performance issues).
		// A-B button status
		buttonStatus = (int)Input.GetAxis ("AButton");
		if (buttonStatus == 1 || abTouchStatus == 1) {
			player.PerformAction (PlayerCharacter.ActionState.Jump);	
			#if UNITY_IPHONE || UNITY_ANDROID
				aButton.Press(Button.ButtonStatus.Pressed);
				bButton.Press(Button.ButtonStatus.Normal);			
			#endif
		} else {
			if (buttonStatus == 0 && abTouchStatus == 0) {
				#if UNITY_IPHONE || UNITY_ANDROID
					aButton.Press(Button.ButtonStatus.Normal);			
					bButton.Press(Button.ButtonStatus.Normal);			
				#endif
			}			
			buttonStatus = (int)Input.GetAxis ("BButton");
			if (buttonStatus == 1 || abTouchStatus == -1) {
				player.PerformAction (PlayerCharacter.ActionState.Cast);	
				#if UNITY_IPHONE || UNITY_ANDROID
					aButton.Press(Button.ButtonStatus.Normal);			
					bButton.Press(Button.ButtonStatus.Pressed);				
				#endif
			}
			if (buttonStatus == 0 && abTouchStatus == 0) {
				#if UNITY_IPHONE || UNITY_ANDROID
					aButton.Press(Button.ButtonStatus.Normal);			
					bButton.Press(Button.ButtonStatus.Normal);			
				#endif
			}					
		}
	}
}