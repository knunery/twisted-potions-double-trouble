using UnityEngine;
using System.Collections;

public class PlayerCharacter : MonoBehaviour
{
	//enum for character state
	public enum ActionState
	{
		Stand=0,
		WalkRight,
		WalkLeft,
		Jump,
		Cast,
		Fall
	};
	
	// Zdepth is used to freeze the z axis.updating it to the 
	// original position on each frame
	public float zDepth;
	
	CharacterController controller;
	private RagePixelSprite ragePixel;

	// Current character state, and secondary/next state that will run.
	public ActionState currentState = ActionState.Stand;
	public ActionState nextState = ActionState.Stand;

	// Used by the sprite to know where is the character facing
	private bool flipState = false;
	
	// Walking vars.
	// The curWalkingspeed is the one that's used in realtime, walkingspeed and airwalking speed 
	// are used to update the curwalkinspeed according to the ocasion (grounded/jumping)	
	private float curWalkingSpeed = 10f;
	public float walkingSpeed = 10f;
	public float airwalkingSpeed = 10f;
	
	// Falling acceleration vars
	private float curFallSpeed = 0f;
	public float FallAccel = 4f;
	public float MaxFallAccel = 18f;

	// Jump length vars
	public float jumpDistance = 700f;
	public float jumpLength = 0.5f;
	private float timeLeft = 0;

	
	private CollisionFlags colFlags;
	public string currentAnimation = "Stand";
	private Vector3 currentPos;
	private Vector3 moveDirection;
	
	void Start ()
	{	
		ragePixel = GetComponent<RagePixelSprite> ();
		zDepth = transform.position.z;	
		controller = GetComponent<CharacterController> ();
		curWalkingSpeed = walkingSpeed;
	}
		
	public void PerformAction (ActionState action)
	{
		// Since Jump and fall are blocking actions we filter them when they're ocurring. This way secondary actions like
		// Walking dont get rewritten by those useless ones that will get ignored later on.
		if (!((currentState == ActionState.Jump || currentState == ActionState.Fall) && action == ActionState.Jump))
			this.nextState = action;			
	}
	
	// Basicly checks if the sent actionstate can move to the left or right and 
	// updates the movedirection attribute on the x axis.
	void SetXVector (ActionState state)
	{
		moveDirection.x = curWalkingSpeed;
		if (state == ActionState.WalkLeft) {
			flipState = false;
			moveDirection.x *= -1;	
		} else if (state == ActionState.WalkRight) {
			flipState = true;
		} else {
			moveDirection.x = 0;	
		}		
	}
	
	void Update ()
	{
		// in case we're suddenly falling and not jumping we'll switch to fall state, plus we'll 
		// setup the char speed for air otherwise we'll return to the grounded state.		
		if (!controller.isGrounded && currentState != ActionState.Fall && currentState != ActionState.Jump) {
			curWalkingSpeed = airwalkingSpeed;
			nextState = ActionState.Fall;
		} else if (controller.isGrounded) { 
			curWalkingSpeed = walkingSpeed;
			curFallSpeed = 0;
		} else if (currentState == ActionState.Jump) {
			curWalkingSpeed = airwalkingSpeed;	
		}
		
		// And now we parse our currentstate and act according to it.
		switch (currentState) {
		case ActionState.Stand:
			// Basically it loops on this state until broken.
			currentAnimation = "Stand";
			ragePixel.PlayNamedAnimation (currentAnimation, false);
			if (nextState != ActionState.Stand)
				currentState = nextState;
			break;
		case (ActionState.Jump):
			// This is a bit hackish but it works since jumps are really short. We use a lerp
			// that goes from timeleft (that goes from 0 to 1) reducing it the timedelta 
			// between frames till it gets to 0. The stored value that has the time constant is jumplength
			// Aside from that we manage the left-right axes too and provide the correct x-axis accel.
			moveDirection = Vector3.zero;
			if (controller.isGrounded)
				timeLeft = 1f * jumpLength;				
			timeLeft = timeLeft - Time.deltaTime;
			moveDirection.y = Mathf.Lerp (0, jumpDistance, timeLeft);
			currentAnimation = "Jump";			
			ragePixel.SetHorizontalFlip (flipState);
			ragePixel.PlayNamedAnimation (currentAnimation, false);
			SetXVector (nextState);
			colFlags = controller.Move (moveDirection * Time.deltaTime);
			if (timeLeft <= 0 || (colFlags == CollisionFlags.CollidedAbove))
				currentState = ActionState.Fall;
			break;
		case (ActionState.Fall):
			// It works similar to the walking algorythm but since we have acceleration we have 
			// an acceleration value. Since we dont want it to accelerate to lightspeed we use a 
			// max value.
			// Aside from that we manage the left-right axes too and provide the correct x-axis accel.
			moveDirection = Vector3.zero;
			if (curFallSpeed > -MaxFallAccel)
				curFallSpeed += FallAccel;
			moveDirection.y -= curFallSpeed; 
			ragePixel.SetHorizontalFlip (flipState);
			SetXVector (nextState);			
			colFlags = controller.Move (moveDirection * Time.deltaTime);
			if (controller.isGrounded)
				currentState = nextState;	
			break;			
		case (ActionState.WalkLeft):
		case (ActionState.WalkRight):
			// Since the only difference is the side we face, we group them, calculate the xvector and flipstate 
			// of movement, play the animation and move.			
			SetXVector (currentState);
			ragePixel.SetHorizontalFlip (flipState);
			currentAnimation = "Walk";
			ragePixel.PlayNamedAnimation (currentAnimation, false);
			colFlags = controller.Move (moveDirection * Time.deltaTime);
			currentState = nextState;
			break;
		case (ActionState.Cast):
			// Another different example, here we freeze all other animations till the play animation is completed.
			if (currentAnimation != "Cast") {
				currentAnimation = "Cast";
				ragePixel.PlayNamedAnimation (currentAnimation, false);
			} else if (!ragePixel.isPlaying () && nextState == ActionState.Cast) {				
				ragePixel.PlayNamedAnimation (currentAnimation, false);
			} else if (!ragePixel.isPlaying () && nextState != ActionState.Cast) {				
				currentState = nextState;
			}
			break;
		}
		// We freeze the Z axis.
		currentPos = transform.position;
		currentPos.z = zDepth;
		transform.position = currentPos;
		
		// If nothing else happens the Nextstate on the next frame will be stand idle.
		nextState = ActionState.Stand;
	}
}