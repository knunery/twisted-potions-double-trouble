using UnityEngine;
using System.Collections;

public class GUIManager : MonoBehaviour
{
	public enum GuiAnchor
	{
		bottomLeft=0,
		bottomRight
	};
	public int offset = 0;
	
	public GuiAnchor anchor = GuiAnchor.bottomLeft;
		
	void Awake ()
	{
		// Let's remove the UI on desktop versions
		// Later versions will only remove the iphone ui not all
		#if UNITY_STANDALONE_WIN || UNITY_STANDALONE_MAC || UNITY_WEBPLAYER
			Destroy(gameObject);
		#endif
		
		// To place this current gui layer on its correct position we'll 
		// check the camera current 0,0 and set it there.
		// It is neccesary that the gui objects are always placed under a camera
		// Since it server for 2 purposes:
		// a) The gui moves along with it as it's parented. 
		// b) It uses it to relocate to it's 0,0
		Camera camera = transform.parent.gameObject.camera;
		switch (anchor) {
			case GuiAnchor.bottomLeft:	
				Vector3 zeroPoint = camera.ViewportToWorldPoint (Vector3.zero);
				zeroPoint.z = gameObject.transform.position.z;
				zeroPoint.x += offset;
				gameObject.transform.position = zeroPoint;			
				break;
			
			case GuiAnchor.bottomRight:	
				Vector3 onePoint = camera.ViewportToWorldPoint (new Vector3(1,0,0));
				onePoint.z = gameObject.transform.position.z;
				onePoint.x += offset;
				gameObject.transform.position = onePoint;
				break;
		}
	}
}